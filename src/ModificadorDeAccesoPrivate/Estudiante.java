package ModificadorDeAccesoPrivate;


import javax.swing.JOptionPane;

public class Estudiante
{
    private String nombres;
    private long cedula;
    private String facultad;

    public Estudiante(String nombres, long cedula,String facultad)
    {
        this.nombres = nombres;
        this.cedula = cedula;
        this.facultad=facultad;
    }

    public Estudiante()
    {
        nombres=JOptionPane.showInputDialog("ingrese el nombre : ");
        cedula=Long.parseLong(JOptionPane.showInputDialog("ingrese el numero de cedula : "));
        facultad=JOptionPane.showInputDialog("ingrese la facultad : ");
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getFacultad() {
        return facultad;
    }

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public String toString()
    {
        return "\nNombre : "+this.nombres+"\nCedula : "+this.cedula+"\nfacultad : "+this.facultad;
    }
}
