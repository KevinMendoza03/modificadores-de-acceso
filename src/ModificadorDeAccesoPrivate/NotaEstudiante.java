package ModificadorDeAccesoPrivate;


import javax.swing.JOptionPane;

public class NotaEstudiante extends Estudiante
{
    private double nota1;
    private double nota2;


    public NotaEstudiante(String nombres, long cedula, String facultad,double nota1,
                          double nota2)
    {
        this.nota1 = nota1;
        this.nota2 = nota2;
    }


    public NotaEstudiante()
    {
        this.nota1=Double.parseDouble(JOptionPane.showInputDialog("ingresar la nota del primer bimestre : "));
        this.nota2=Double.parseDouble(JOptionPane.showInputDialog("ingresar la nota del segundo bimestre "));
    }


    public double promedio()
    {
        double notaTotal;
        notaTotal=(nota1+nota2)/2;
        return notaTotal;
    }

    public String toString()
    {
        return super.toString()+"\nnota 1 : "+this.nota1 +"\nnota2 : "+this.nota2;
    }

}
