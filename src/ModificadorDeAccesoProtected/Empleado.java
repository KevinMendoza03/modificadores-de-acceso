package ModificadorDeAccesoProtected;


import javax.swing.JOptionPane;

public class Empleado
{
    protected String nombres;
    protected long cedula;

    public Empleado(String nombres, long cedula)
    {
        this.nombres = nombres;
        this.cedula = cedula;
    }

    public Empleado()
    {
        nombres=JOptionPane.showInputDialog("ingrese el nombre : ");
        cedula=Long.parseLong(JOptionPane.showInputDialog("ingrese el numero de cedula : "));
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String toString()
    {
        return "\nNombre : "+this.nombres+"\nCedula : "+this.cedula;
    }
}
