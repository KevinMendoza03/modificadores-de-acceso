package ModificadorDeAccesoProtected;


import javax.swing.JOptionPane;

public class Empleado_Hora extends Empleado
{
    protected double hora;
    protected double valorHora;


    public Empleado_Hora(String nombres, long cedula, double hora,
                         double valorHora)
    {
        this.hora = hora;
        this.valorHora = valorHora;
    }


    public Empleado_Hora()
    {
        this.hora=Double.parseDouble(JOptionPane.showInputDialog("ingresar numeros de horas trabajadas : "));
        this.valorHora=Double.parseDouble(JOptionPane.showInputDialog("ingresar el valor por horas : "));
    }


    public double getHora() {
        return hora;
    }


    public void setHora(double hora) {
        this.hora = hora;
    }


    public double getValorHora() {
        return valorHora;
    }


    public void setValorHora(double valorHora) {
        this.valorHora = valorHora;
    }

    //calculo
    public float calculoSalario2()
    {
        float salTotal;
        salTotal=(float) (this.valorHora*this.hora)*20;
        return salTotal;
    }

    public String toString()
    {
        return super.toString()+"\nHoras trabajadas : "+this.hora+"\nValor horas : "+this.valorHora;
    }

}
