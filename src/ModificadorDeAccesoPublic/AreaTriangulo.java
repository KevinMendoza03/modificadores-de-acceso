package ModificadorDeAccesoPublic;

import java.util.Scanner;

public class AreaTriangulo {

    public float altura;
    public float base;


    public float area(){
        return (base*altura)/2;
    }

    public AreaTriangulo(float altura, float base) {
        this.altura = altura;
        this.base = base;
    }

    @Override
    public String toString() {
        return "Area triangulo = (base * altura) /2 \n" +
                "base =" + base+
                ",   altura=" + altura ;
    }

    public static void main(String[] args){

        Scanner triangulo = new Scanner(System.in);
        float base,altura,area;
        AreaTriangulo a;
        System.out.print("Ingrese base : ");
        base=triangulo.nextFloat();
        System.out.print("Ingrese altura: ");
        altura=triangulo.nextFloat();
        a=new AreaTriangulo(base,altura);
        System.out.println(a);
        System.out.println("area: "+a.area());
    }

}
